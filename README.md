# Vefi - The AI project

## Setting up

Only linux distribution supported right now

### Prerequisites
VSCode
VSCode Extensions:
 -C/C++ by Microsoft
 - C++ Intellisense by Austin
 - Code Runenr by Jun Han
 - Easy C++ Projects by Alejandro Charte Luque
 - Git Lense

1. Pull Down Repo
2. Install Clang
```
 sudo apt install clang

```
3. Install dolphin-emu
```
sudo apt install dolphin-emu
```

4. Add 'bin' folder next to src (this folder is not tracked an unit tests will fail to find the directory)
5. Install smash iso and point iso path to it (dolphin fixture)

    
