/*Fix the errors and get an error free compilation and execution.*/

#include "main.hpp"

/*Goal: practice if-else statements in C++
**Write a program to select the best pet. 
*/

#include<iostream>

using namespace std;

bool debug = false;

string PROCESS_DIRECTORY(void)
{
    return "/proc";
}

std::string printFile(string path) {
  fstream file;
  file.open(path);
  string currentLine = "";
  string fileContents = "";
  while (getline(file,currentLine))
  {
    fileContents += currentLine; 
  }
  file.close();
  return fileContents;
}

int getDolphinPid()
{
    //open
    char *directory = &PROCESS_DIRECTORY()[0];
    DIR *dir;
    struct dirent *directoryEntry = NULL;
    dir = opendir(directory);
    if (!dir) {
      cout << "Directory not found at "<< directoryEntry->d_name << endl;
      return 0;
    }
    /* print all the files and directories within directory */

    int dolphinProcessId = 0;
    //read
    while (((directoryEntry = readdir (dir)) != NULL) && !dolphinProcessId) {
        
        int currentPid = 0;
        std::istringstream conversionStream(directoryEntry->d_name);
        conversionStream >> currentPid;
        if(currentPid)
        {
            string path = string(directory) + '/' + to_string(currentPid) + "/comm";
            
            if(debug) cout << currentPid << ": ";

            string fileContents = printFile(path);
            if(fileContents.find("dolphin-emu") != std::string::npos) {
              if(debug) cout << fileContents;
              dolphinProcessId = currentPid;
            }    

            if(debug) cout << endl;
        } else {
          if(debug) cout << "Could not convert read Pid into an int" << endl;
        }
    }

    //close
    closedir(dir);
    
    return dolphinProcessId;
}


// Comment out to run unit tests
// int main()
// {
//   int dolphinID = getDolphinPid();
//   cout << "Dolphin pid is: " << dolphinID << endl;
//   return 0;
// }
