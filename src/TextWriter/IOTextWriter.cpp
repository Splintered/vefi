#include "IOTextWriter.hpp"

int main() {
    string currentLine = "";

    fstream myfile;

    myfile.open("input.txt", ios::app );

    myfile << '\n' <<"Never gonna make you cry" << endl;
    myfile << "Never gonna say goodbye" << endl;
    myfile << "Never gonna tell a lie" << endl;

    myfile.close();

    myfile.open("input.txt", ios::in);

    while( getline( myfile, currentLine))
    {
        cout << currentLine << endl;
    }


    myfile.close(); 
}

