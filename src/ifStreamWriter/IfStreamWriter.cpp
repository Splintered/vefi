/*Fix the errors and get an error free compilation and execution.*/

#include "IfStreamWriter.h"

/*Goal: practice if-else statements in C++
**Write a program to select the best pet. 
*/

#include<iostream>

using namespace std;

string PROCESS_DIRECTORY = "/proc"; 

std::string printFile(string path) {
  fstream file;
  file.open(path);
  string currentLine = "";
  string fileContents = "";
  while (getline(file,currentLine))
  {
    fileContents += currentLine; 
  }
  file.close();
  return fileContents;
}



int getDolphinPid(bool debug = false)
{
    //open
    char *directory = &PROCESS_DIRECTORY[0];
    DIR *dir;
    struct dirent *directoryEntry;
    dir = opendir(directory);
    if (!dir) {
      cout << "Directory not found at "<< directoryEntry->d_name << endl;
      return 0;
    }
    /* print all the files and directories within directory */

    int dolphinProcessId = 0;
    //read
    while (((directoryEntry = readdir (dir)) != NULL) && !dolphinProcessId) {
        
        int currentPid = 0;
        std::istringstream conversionStream(directoryEntry->d_name);
        
        //if read string could not be converted to int skip it
        if(!(conversionStream >> currentPid)) continue;

        if(currentPid)
        {
            string path = string(directory) + '/' + to_string(currentPid) + "/comm";
            
            cout << currentPid << ": ";

            string fileContents = printFile(path);
            if(fileContents.find("dolphin-emu") != std::string::npos) {
              
              if(debug) {cout << fileContents;}
              
              dolphinProcessId = currentPid;
            }    

            cout << endl;
        } else {
          cout << "Skipped" << endl;
        }
    }

    //close
    closedir (dir);
    
    return dolphinProcessId;
}

int main()
{
  int dolphinID = getDolphinPid();
  cout << "Dolphin pid is: " << dolphinID << endl;
  cout << "========== Starting Ram Info Read =========="<< endl;
  
  std::ifstream theMapsFile("/proc/" + std::to_string(dolphinID) + "/maps");
  std::string line;
  bool MEM1Found = false;

  unsigned long int m_emuRAMAddressStart = 0;
  bool m_MEM2Present = false;

  int linePosition = 0;
  while (getline(theMapsFile, line))
  {

    cout << linePosition++ << ": " << line << endl;

    if (line.length() > 73)
    {
      cout << line.length() << ": " << line.substr(73, 19) << endl;

      if (line.substr(73, 19) == "/dev/shm/dolphinmem" ||
          line.substr(73, 20) == "/dev/shm/dolphin-emu")
      {
        cout<< "Line: " << line << endl;
        unsigned long int firstAddress = 0;
        unsigned long int secondAddress = 0;
        std::string firstAddressStr("0x" + line.substr(0, 12));
        std::string secondAddressStr("0x" + line.substr(13, 12));

        firstAddress = std::stoul(firstAddressStr, nullptr, 16);
        
        secondAddress = std::stoul(secondAddressStr, nullptr, 16);

        if (MEM1Found)
        {
          if (firstAddress == m_emuRAMAddressStart + 0x10000000)
          {
            m_MEM2Present = true;
            break;
          }
          else if (firstAddress > m_emuRAMAddressStart + 0x10000000)
          {
            m_MEM2Present = false;
            break;
          }
          continue;
        }

        unsigned long int test = secondAddress - firstAddress;

        if (secondAddress - firstAddress == 0x2000000)
        {
          m_emuRAMAddressStart = firstAddress;
          MEM1Found = true;
        }
      }
    }
  }
  return 0;
}