
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "main.hpp"
#include "dolphinFixture.hpp"
#include <future>
#include <string>
#include <iostream>

unsigned int Factorial( unsigned int number ) {
    return number <= 1 ? number : Factorial(number-1)*number;
}
using namespace std;
int timeOut = 20;
int pollingCadence = 1;
TEST_CASE( "Should Start Dolphin Process", "[Start]" ) {
    future<int> result_future = async(Start);

    cout << "Getting ID" << endl;
    int processID = getDolphinPid();

    //Polling Wait
    while(timeOut-=pollingCadence > 0 )
    {
        cout << "Entering Polling Loop" << timeOut << ": " << processID << endl;
        if(processID > 0) break;
        
        sleep(pollingCadence);
        processID = getDolphinPid();
    }

    REQUIRE(processID > 0);

    // Ensure that Confirm on stop is disabled in the UI other wise process will hang on confirmation box
    
    Stop();
}

TEST_CASE( "Read file should return the contents of a file", "[printFile]" ) {
    string expectedContents = "Never Gonna Give You Up";
    string actualContents = printFile("./assets/TestFile");
    REQUIRE(expectedContents == actualContents);
}