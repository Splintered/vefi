import os

print('hello world')

mainDirectory = '/proc'


pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]

for pid in pids:
  processDirectory = mainDirectory + '/%s/comm'  % pid
  file = open(processDirectory,'r') 

  processName = '' 

  contents = str(file.read())
  if contents.find('dolphin') > -1:
    processName = contents
  
  print(pid + ': ' + processName)

  file.close()

print('Someone setup us the bomb')
